#!/usr/bin/env python3
from collections import Counter
import sqlite3
import sys

db = sqlite3.connect("out.sqlite")

c = db.execute('''
    select 
        distinct attr
    from attr_names 
    where
        drv_path in (
            select
                src
            from refs
            where
                dst in (
                    select 
                        drv_path
                    from attr_names
                        where
                            attr = ? and 
                            system = 'x86_64-linux'
                )
        )
    order by
        attr      
''', (sys.argv[1], ))

print('''
let
    pkgs = import ./. {};
in pkgs.mkShell {
    packages = with pkgs; [
''')
for package, in c:
    print(" " * 6 + package)
print('''
    ];
}
''')
