use std::{
    collections::HashSet,
    io::{BufRead, BufReader},
    process::{Command, Stdio},
};

use indicatif::{ParallelProgressIterator, ProgressBar, ProgressStyle};
use rayon::prelude::{ParallelBridge, ParallelIterator};
use rusqlite::{Connection, DatabaseName};
use serde::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
struct Line {
    attr: Option<String>,
    drv_path: Option<String>,
}

fn make_db() -> Connection {
    let conn = Connection::open("file::memory:?cache=shared").unwrap();

    conn.execute(
        "CREATE TABLE attr_names (attr TEXT NOT NULL, drv_path TEXT NOT NULL, system TEXT NOT NULL)",
        (),
    )
    .unwrap();
    conn.execute("CREATE INDEX attr_names_by_name ON attr_names(attr)", ())
        .unwrap();
    conn.execute(
        "CREATE INDEX attr_names_by_path ON attr_names(drv_path)",
        (),
    )
    .unwrap();

    conn.execute(
        "CREATE TABLE refs (src TEXT NOT NULL, dst TEXT NOT NULL)",
        (),
    )
    .unwrap();
    conn.execute("CREATE INDEX refs_by_src ON refs(src)", ())
        .unwrap();
    conn.execute("CREATE INDEX refs_by_dst ON refs(dst)", ())
        .unwrap();

    conn
}

fn eval_for_system(system: &str) {
    let output = Command::new("nix-eval-jobs")
        .arg("--workers")
        .arg("6")
        .arg("/home/k900/gh/NixOS/nixpkgs")
        .arg("--system")
        .arg(system)
        .stdout(Stdio::piped())
        .stderr(Stdio::null())
        .spawn()
        .unwrap();

    let reader = BufReader::new(output.stdout.unwrap());

    let bar = ProgressBar::new_spinner();
    bar.set_style(
        ProgressStyle::with_template("{spinner:.green} {prefix}: {wide_msg} total: {pos:>4}")
            .unwrap(),
    );
    bar.set_prefix(format!("Evaluating {}", system));

    reader
        .lines()
        .par_bridge()
        .progress_with(bar.clone())
        .for_each_init(
            || Connection::open("file::memory:?cache=shared").unwrap(),
            |conn: &mut Connection, line| {
                let line = line.unwrap();
                let line: Line = serde_json::from_str(&line).unwrap();

                let Some(attr) = line.attr else { return };
                let Some(drv_path) = line.drv_path else { return };

                conn.execute(
                    "INSERT INTO attr_names(attr, drv_path, system) VALUES (?1, ?2, ?3)",
                    (&attr, &drv_path, system),
                )
                .unwrap();

                bar.set_message(attr);

                let Ok(child) = Command::new("nix-store")
            .arg("--query")
            .arg("--references")
            .arg(&drv_path)
            .stdout(Stdio::piped())
            .spawn() else { return };

                let Ok(output) = child.wait_with_output() else { return };

                let references: HashSet<_> = output
                    .stdout
                    .lines()
                    .flatten()
                    .filter(|x| x.ends_with(".drv"))
                    .collect();

                for item in references {
                    conn.execute(
                        "INSERT INTO refs(src, dst) VALUES (?1, ?2)",
                        (&drv_path, &item),
                    )
                    .unwrap();
                }

                bar.inc(1);
            },
        );
}

fn main() {
    let conn = make_db();

    for system in [
        "x86_64-linux",
        "aarch64-linux",
        "x86_64-darwin",
        "aarch64-darwin"
    ] {
        eval_for_system(system);
    }

    conn.backup(DatabaseName::Main, "out.sqlite", None).unwrap();
}
