#!/usr/bin/env python3
from collections import Counter
import sqlite3
import sys

db = sqlite3.connect("out.sqlite")

seen = set()
systems = {}
depths = {}

if len(sys.argv) == 2:
    query = "SELECT drv_path, system FROM attr_names WHERE attr = ?", (sys.argv[1], )
elif len(sys.argv) == 3:
    query = "SELECT drv_path, system FROM attr_names WHERE attr = ? AND system = ?", (sys.argv[1], sys.argv[2])
else:
    raise TypeError

for path, system in db.execute(*query):
    seen.add(path)
    depths[path] = 0
    systems[path] = system

counts = Counter()
parents = {}

def walk(paths, depth=1):
    paths_s = ",".join(f"'{path}'" for path in paths)
    next_front = set()

    for src, dst in db.execute(f"SELECT refs.src, refs.dst FROM refs WHERE refs.dst in ({paths_s})"):
        if src in seen:
            continue
        depths[src] = depth
        seen.add(src)
        next_front.add(src)

        parents[src] = dst
        here = src
        systems[src] = systems[dst]
        while here:
            counts[here] += 1
            here = parents.get(here)

    if next_front:
        walk(next_front, depth=depth + 1)

walk(seen)
for k, n in counts.most_common():
    if depths[k] <= 1 and n > 1:
        print(k, systems.get(k, ""), n)
